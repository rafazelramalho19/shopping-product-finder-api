FROM node:10.10.0-alpine

# Update the OS
RUN apk update && apk upgrade

# Install NodeJS
RUN apk add nodejs

# Remove APK cache
RUN rm -rf /var/cache/apk/*

# Copy current directory content to src inside the container
COPY . .

WORKDIR /src
RUN npm install
EXPOSE 3000
CMD ["node", "/src/app.js"]