'use strict';

const Knex = require('knex');
const KnexFile = require('./knexfile');
const { Model } = require('objection');

const env = process.env.NODE_ENV || 'development';
const config = KnexFile[env];
const knexInstance = Knex(config);
Model.knex(knexInstance);

module.exports = {
    register: (server) => server.expose('database', knexInstance)
};
