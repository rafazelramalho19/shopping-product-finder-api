'use strict';

const tableName = 'stores';

exports.up = function (knex, Promise) {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();
        table.string('name').unique();

        table.integer('location_id').unsigned();
        table.foreign('location_id').references('id').inTable('locations');
        table.timestamps();
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists(tableName);
};
