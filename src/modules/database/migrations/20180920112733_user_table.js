'use strict';

const tableName = 'users';

exports.up = function (knex, Promise) {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();
        table.string('name');
        table.string('email').unique();
        table.string('password');
        table.string('role');

        table.timestamps();
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists(tableName);
};
