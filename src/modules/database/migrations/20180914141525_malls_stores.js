'use strict';

const tableName = 'malls_stores';

exports.up = function (knex, Promise) {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();

        table.integer('mall_id').unsigned();
        table.foreign('mall_id').references('id').inTable('malls');

        table.integer('store_id').unsigned();
        table.foreign('store_id').references('id').inTable('stores');

        table.integer('location_id').unsigned();
        table.foreign('location_id').references('id').inTable('locations');
        table.timestamps();
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists(tableName);
};
