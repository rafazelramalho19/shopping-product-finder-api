'use strict';

const tableName = 'stores_products';

exports.up = function (knex, Promise) {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();

        table.integer('product_id').unsigned();
        table.foreign('product_id').references('id').inTable('products');

        table.integer('store_id').unsigned();
        table.foreign('store_id').references('id').inTable('stores');

        table.integer('availability').default(0);
        table.string('price').nullable();

        table.timestamps();
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists(tableName);
};
