'use strict';

const tableName = 'tokens';

exports.up = function (knex, Promise) {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();
        table.string('token');

        table.integer('user_id').unsigned();
        table.foreign('user_id').references('id').inTable('users');

        table.timestamps();
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists(tableName);
};
