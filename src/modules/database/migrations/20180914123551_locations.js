'use strict';

const tableName = 'locations';

exports.up = function (knex, Promise) {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();
        table.string('longitude');
        table.string('latitude');
        table.string('altitude').nullable();
        table.timestamps();
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists(tableName);
};
