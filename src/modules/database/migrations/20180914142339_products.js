'use strict';

const tableName = 'products';

exports.up = function (knex, Promise) {
    return knex.schema.createTable(tableName, (table) => {
        table.increments('id').primary();
        table.string('name').nullable();
        table.text('description').nullable();
        table.string('barcode').nullable();
        table.string('identifier').nullable();

        table.timestamps();
    });
};

exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists(tableName);
};
