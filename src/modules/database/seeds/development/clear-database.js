'use strict';

const tables = [
    'malls',
    'stores',
    'products',
    'malls_stores',
    'stores_products',
    'locations'
];

exports.seed = async (knex, Promise) => {
    await tables.map(async (table) => await knex.table(table).del());
};
