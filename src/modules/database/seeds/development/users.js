'use strict';

const Faker = require('faker');
const Bcrypt = require('bcrypt');

const tableName = 'users';
const SEED_AMMOUNT = 10;

const createUsers = async (knex) => {
    const firstName = Faker.name.firstName();
    const lastName = Faker.name.lastName();

    const salt = await Bcrypt.genSalt(10);
    // hash the password along with our new salt
    const password = await Bcrypt.hash('test password', salt);

    return await knex.table(tableName).insert([{
        name: `${firstName} ${lastName}`,
        email: Faker.internet.email(firstName, lastName, 'blip.pt').toLowerCase(),
        password
    }]);
};

exports.seed = async (knex, Promise) => {
    // Deletes ALL existing entries
    await knex.table(tableName).del();

    const creationPromises = new Array(SEED_AMMOUNT)
        .fill('', 0, SEED_AMMOUNT)
        .map(() => createUsers(knex));
    await Promise.all(creationPromises);
};
