'use strict';

const faker = require('faker');

const tableName = 'locations';
const SEED_AMOUNT = 10;

const createLocation = async (knex) => await knex.table(tableName).insert([{
    longitude: faker.address.longitude(),
    latitude: faker.address.latitude()
}]);

exports.seed = async (knex, Promise) => {
    // Deletes ALL existing entries
    await knex.table(tableName).del();

    const creationPromises = new Array(SEED_AMOUNT)
        .fill('', 0, SEED_AMOUNT)
        .map(() => createLocation(knex));
    await Promise.all(creationPromises);
};
