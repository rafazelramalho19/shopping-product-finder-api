'use strict';

const faker = require('faker');

const tableName = 'malls';
const SEED_AMMOUNT = 10;

const createMall = async (knex) => {
    const firstLocation = await knex.table('locations').first('id');
    return await knex.table(tableName).insert([{
        name: faker.name.firstName(),
        location_id: firstLocation.id
    }]);
};

exports.seed = async (knex, Promise) => {
    // Deletes ALL existing entries
    await knex.table(tableName).del();

    const creationPromises = new Array(SEED_AMMOUNT)
        .fill('', 0, SEED_AMMOUNT)
        .map(() => createMall(knex));
    await Promise.all(creationPromises);
};
