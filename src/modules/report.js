'use strict';

const Good = require('good');

const options = {
    ops: {
        interval: 1000
    },
    reporters: {
        myConsoleReporter: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ error: '*' }]
        }, {
            module: 'good-console'
        }, 'stderr']
    }
};

module.exports = [{
    plugin: Good,
    options
}];
