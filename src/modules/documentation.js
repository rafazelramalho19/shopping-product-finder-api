'use strict';

const Pack = require('../package');

module.exports = [
    require('vision'),
    require('inert'),
    {
        plugin: require('hapi-swagger'),
        options: {
            info: {
                title: 'Shopping Product API Documentation',
                version: Pack.version
            },
            securityDefinitions: {
                'jwt': {
                    'type': 'apiKey',
                    'name': 'Authorization',
                    'in': 'header'
                }
            },
            security: [{ 'jwt': [] }]
        }
    }
];
