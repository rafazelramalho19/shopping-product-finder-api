'use strict';

const Database = require('./database/index');

const options = {};

module.exports = [{
    plugin: {
        ...Database,
        name: 'Database'
    },
    options
}];
