'use strict';

const Fs = require('fs');

const plugins = [];

Fs.readdirSync(__dirname)
    .filter((file) => file !== 'index.js' && file.indexOf('.js') !== -1)
    .map((file) => plugins.push(...require(`./${file}`)));

module.exports = plugins;
