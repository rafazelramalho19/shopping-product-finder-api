'use strict';

const { Model } = require('objection');

module.exports = class Location extends Model {
    static get tableName() {
        return 'locations';
    }

    static get relationMappings() {
        return {
            malls: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/mall',
                join: {
                    from: 'location.id',
                    to: 'mall.location_id'
                }
            }
        };
    }
};
