'use strict';

const { Model } = require('objection');

module.exports = class Mall extends Model {
    static get tableName() {
        return 'malls';
    }

    static get relationMappings() {
        return {
            location: {
                relation: Model.HasOneRelation,
                modelClass: __dirname + '/location',
                join: {
                    from: 'location.id',
                    to: 'malls.location_id'
                }
            }
        };
    }
};
