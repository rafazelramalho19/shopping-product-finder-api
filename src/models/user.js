'use strict';

const { Model } = require('objection');
const Bcrypt = require('bcrypt');
const JWT = require('jsonwebtoken');

module.exports = class User extends Model {
    static get tableName() {
        return 'users';
    }

    $toJson(){
        // Ommit the password and role field from responses
        const { password, role, ...otherFields } = this;
        return otherFields;
    }

    static async authenticate(password, user) {
        return await Bcrypt.compare(password, user.password);
    }

    static async generateJWT({ email }) {
        const token = await JWT.sign({ email }, process.env.SECRET_KEY);

        return token;
    }

    static isEditor({ role }) {
        return role === 'editor';
    }

    static get relationMappings() {
        return {
            tokens: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/token',
                join: {
                    from: 'users.id',
                    to: 'tokens.user_id'
                }
            }
        };
    }
};
