'use strict';

const { Model } = require('objection');

module.exports = class Token extends Model {
    static get tableName() {
        return 'tokens';
    }

    static get relationMappings() {
        return {
            user: {
                relation: Model.HasOneRelation,
                modelClass: __dirname + '/user',
                join: {
                    from: 'users.id',
                    to: 'tokens.user_id'
                }
            }
        };
    }

    static async create(token, { id }) {
        return await this.query().insert({
            token,
            user_id: id,
            created_at: new Date(),
            updated_at: new Date()
        });
    }
};
