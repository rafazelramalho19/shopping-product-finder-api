'use strict';

const Path = require('path');

module.exports = {
    getEnvLocation() {
        const path = Path.join(__dirname, '..', '..', '.env');
        return require('dotenv').config({ path });
    }
};
