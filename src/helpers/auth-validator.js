'use strict';

const User = require('../models/user');
const Token = require('../models/token');

const isInvalid = () => ({ isValid: false });
const isValid = () => ({ isValid: true });

module.exports = {
    async validate({ email }, request) {
        const user = await User.query()
            .where('email', email).first();

        if (!user) {
            return isInvalid();
        }

        const authToken = request.headers.authorization;
        const token = await Token.query().where('token', authToken).first();

        if (!token) {
            return isInvalid();
        }

        if (token.user_id !== user.id) {
            return isInvalid();
        }

        return isValid();
    }
};
