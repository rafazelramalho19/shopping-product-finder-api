'use strict';

module.exports = {
    getMiddlewares: (middlewares) => ({
        pre: middlewares
            .map((middleware) => require(`./${middleware}.js`))
            .map((middleware) => [{ method: middleware }])
    })
};
