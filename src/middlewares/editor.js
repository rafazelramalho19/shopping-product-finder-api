'use strict';

const Boom = require('boom');
const User = require('../models/user');

// This middleware aims to allow a certain user with edition powers to CRUD entities.
const unauthorized = () => Boom.unauthorized('You dont have permisions to access this route!');

module.exports = async ({ auth }) =>  {
    if (!auth.isAuthenticated) {
        return unauthorized();
    }

    const { credentials } = auth;
    if (!credentials) {
        return unauthorized();
    }

    const { email } = credentials;
    if (!email) {
        return unauthorized();
    }

    const user = await User.query().where('email', email).eager('tokens').first();
    if (!user) {
        return unauthorized();
    }

    const { tokens } = user;

    if (!tokens) {
        return unauthorized();
    }

    const userTokens = tokens.map((token) => token.token).filter((token) => token === auth.token);

    if (!userTokens.length === 0) {
        return unauthorized();
    }

    if (!User.isEditor(user)) {
        return unauthorized();
    }

    return true;
};
