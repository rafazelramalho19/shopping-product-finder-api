'use strict';

const Joi = require('joi');
const UserController = require('../controllers/user.controller');
const { getMiddlewares } = require('../middlewares');

module.exports = [{
    method: 'POST',
    path: '/login',
    handler: UserController.login,
    options: {
        tags: ['api', 'user', 'auth'],
        validate: {
            payload: {
                email: Joi.string().email().required(),
                password: Joi.string().required()
            }
        }
    }
},{
    method: 'POST',
    path: '/users/edit',
    handler: UserController.edit,
    options: {
        auth: 'jwt',
        tags: ['api', 'user', 'auth'],
        validate: {
            payload: {
                email: Joi.string().email().required(),
                name: Joi.string().required()
            }
        },
        ...getMiddlewares(['editor'])
    }
}];
