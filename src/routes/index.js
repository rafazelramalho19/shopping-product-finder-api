'use strict';

const Fs = require('fs');

const routes = [];

Fs.readdirSync(__dirname)
    .filter((file) => file !== 'index.js')
    .map((file) => routes.push(...require(`./${file}`)));

module.exports = routes;
