'use strict';

const MallController = require('../controllers/mall.controller');

module.exports = [{
    method: 'GET',
    path: '/malls',
    handler: MallController.getMalls,
    options: {
        tags: ['api', 'malls']
    }
}];
