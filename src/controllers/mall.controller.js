'use strict';

const Mall = require('../models/mall');

class MallController {
    async getMalls() {
        return await Mall.query();
    }

    async createMall(mall) {
        return await Mall.query().insert(mall);
    }
}

module.exports = new MallController();
