'use strict';

const User = require('../models/user');
const Token = require('../models/Token');
const Boom = require('boom');

class UserController {
    async login(request) {
        const { payload } = request;
        const { email, password } = payload;

        const user = await User.query().where('email', email).first();

        if (!user) {
            return Boom.badRequest('Invalid credentials');
        }

        const isValidPassword = await User.authenticate(password, user);

        if (!isValidPassword) {
            return Boom.badRequest('Invalid credentials');
        }

        const token = await User.generateJWT({ ...user });

        await Token.create(token, user);

        return token;
    }

    async edit(request) {
        const { payload } = request;
        const { email, name } = payload;

        const user = await User.query().where('email', email).first();

        if (!user) {
            return Boom.badRequest('User not found');
        }

        const updatedUser = await User.query().updateAndFetchById(user.id, {
            name
        });

        if (!updatedUser) {
            return Boom.badData();
        }

        return updatedUser;
    }
}

module.exports = new UserController();
