'use strict';

const Hapi = require('hapi');
const Routes = require('./routes');
const Modules = require('./modules');
const AuthValidatior = require('./helpers/auth-validator');

// Load environment variables
require('./helpers/environment').getEnvLocation();

const baseConfig = process.env.ENVIRONMENT === 'development' ? { host: 'localhost' } : {};
const server = Hapi.server({
    ...baseConfig,
    port: 3000,
    debug: { request: ['error'] }
});

const initServer = async () => {
    await server.register(Modules);
    await server.auth.strategy('jwt', 'jwt', {
        key: process.env.SECRET_KEY,
        validate: AuthValidatior.validate,
        verifyOptions: { algorithms: ['HS256'] }
    });
    await server.route(Routes);

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

initServer();

process.on('unhandledRejection', (err) => {
    console.error(err);
    process.exit(1);
});
