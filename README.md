# shopping-product-finder-api

# Migrations

All the migrations are located in `src/modules/database/migrations`. All the operations regarding migrations should be done inside the database folder

Please always do `cd src/modules/database/` before doing any knex commands.
For more information please read https://knexjs.org/#Migrations-CLI.

## First time running the project

Run migrations for database generation
> knex migrate:latest

## Rollbacking

Useful when making mistakes in the migrations
> knex migrate:rollback

## Creating a new migration

> knex migrate:make \<name\>


