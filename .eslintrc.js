module.exports = {
    "extends": "hapi",
    "rules": {
        "hapi/hapi-scope-start": ['allow-one-liners']
    },
    "parserOptions": {
        "ecmaVersion": 2018
    }
};